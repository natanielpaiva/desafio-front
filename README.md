Front 
===

O projeto front-end está utilizando as seguintes tecnologias:

* SPA
* Carregamento lazy load de rotas(só carrega o que é chamado pelo usuário através do menu)
* Angular 7.2
* CRUD simples
* Autenticação, guardo o token através do sessionStorage.

Com relação ao Angular, utilizei o máximo de recursos possíveis(no tempo que tive) como:

* Components
* Services
* Pipes
* Directives
* Interceptors
* FormReactive 


O projeto também está com integração contínua utilizando o CI do Gitlab o DPL do travis e a AWS da Amazon. Segue o link do mesmo:

[Link do projeto front](http://front.us-east-2.elasticbeanstalk.com/#/home)