import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddressComponent } from './address.component';
import { AddressListComponent } from './list/address-list.component';
import { AddressDetailComponent } from './detail/address-detail.component';

const routes: Routes = [
  { 
    path: '',
    component:AddressComponent
  },
  { 
    path: 'edit/:id',
    component:AddressComponent
  },
  { 
    path: 'detail/:id',
    component:AddressDetailComponent
  },
  { 
    path: 'list',
    component:AddressListComponent
  }

];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [ RouterModule ]
})
export class AddressRoutingModule { }
