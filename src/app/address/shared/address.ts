export class Address {
    id:number = null;
    cep:string
    cidade:string
    logradouro:string
    complemento:string
    bairro:string
    localidade:string
    uf:string
}
