import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Address } from './address';
import { AddressModule } from '../address.module';

@Injectable({
  providedIn: 'root'
})
export class AddressService {
  private url = environment.backendUrl + "address"
  constructor(private http: HttpClient) { }

  public save(address: Address): Observable<any> {
    if(address.id){
      return this.http.put<any>(this.url, address);
    }
    return this.http.post<any>(this.url, address);
  }
  
  public edit(address: Address): Observable<any> {
    return this.http.put<any>(this.url, address);
  }
  
  public getById(id:number): Observable<any> {
    return this.http.get<any>(this.url + "/" + id);
  }
 
  public findAll(): Observable<any> {
    return this.http.get<any>(this.url + "?page=0&size=100");
  }

  public delete(id:number):Observable<any>{
    return this.http.delete(this.url + "/" + id)
  }


}
