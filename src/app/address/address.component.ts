import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UfService } from '../uf/shared/uf.service';
import { ViacepService } from '../viacep/shared/viacep.service';
import { AddressService } from './shared/address.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css'],
  providers: [UfService, ViacepService]
})
export class AddressComponent implements OnInit {

  public form: FormGroup
  public invalid = false
  public ufs = []
  public id: number


  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private addressService: AddressService,
    private viaCepService: ViacepService,
    private ufService: UfService,
    private route: ActivatedRoute,
  ) {
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      id:[null],
      cep: ['', [Validators.required]],
      logradouro: ['', [Validators.required]],
      bairro: ['', [Validators.required]],
      cidade: ['', [Validators.required]],
      uf: ['', [Validators.required]],
      complemento: [''],
    });
  }

  public submit() {
    if (!this.form.invalid) {
      this.addressService.save(this.form.value).subscribe(
        response => {
          this.router.navigate(['/address/list']);
        }
        , error => {
          this.invalid = true
        }
      );
    }
  }

  close() {
    this.invalid = false
  }

  get f() { return this.form.controls; }

  ngOnInit() {
    this.buildForm()
    this.ufService.findAll().subscribe(
      response => {
        this.ufs = response
        this.loadById()
      }
    )

  }

  private loadById() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.addressService.getById(this.id).subscribe(
          response => {
            this.form.patchValue(response)
          }
        )
      }
    })
  }

  toBack(){
    if (this.id) {
      return "/address/list"
    }
    return "/home"
  }

  searchCep() {
    if (this.f.cep.value.length == 8) {
      this.viaCepService.getCep(this.f.cep.value).subscribe(
        response => {
          this.form.controls['cidade'].setValue(response.localidade)
          this.form.patchValue(response)
        }
      )
    }
  }

}
