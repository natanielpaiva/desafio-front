import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Address } from '../shared/address';
import { AddressService } from '../shared/address.service';

@Component({
  selector: 'app-address-details',
  templateUrl: './address-detail.component.html',
})
export class AddressDetailComponent implements OnInit {

  public address: Address
  public id: number

  constructor(
    private addressService: AddressService,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
      this.loadById()
  }

  private loadById() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.addressService.getById(this.id).subscribe(
          response => {
            this.address = response 
          }
        )
      }
    })
  }

}
