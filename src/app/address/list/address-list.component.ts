import { Component, OnInit } from '@angular/core';
import { AddressService } from '../shared/address.service';
import { Role } from 'src/app/shared/roles/role';

@Component({
  selector: 'app-address-list',
  templateUrl: './address-list.component.html'
})
export class AddressListComponent implements OnInit {
  public addressList = []
  public roles = [Role.ADMIN]
  public deleted = false

  constructor(private addressService:AddressService) { }

  ngOnInit() {
    this.findAll()
  }

  public findAll(){
    this.addressService.findAll().subscribe(
      response => {
        this.addressList = response.content
      }
    )
  }

  public close(){
    this.deleted = false
  }

  public delete(id:number){

    this.addressService.delete(id).subscribe(
      response =>{ 
        this.deleted = true
        this.findAll()
      }
    )
  }

}
