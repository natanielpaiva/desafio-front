import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressComponent } from './address.component';
import { AddressListComponent } from './list/address-list.component';
import { AddressRoutingModule } from './address.routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { PipeModule } from '../shared/pipes/pipe.module';
import {NgxMaskModule} from 'ngx-mask'
import { AddressDetailComponent } from './detail/address-detail.component';
import { DirectivesModule } from '../shared/directives/directives.module';

@NgModule({
  declarations: [AddressComponent, AddressListComponent, AddressDetailComponent],
  imports: [
    CommonModule,
    AddressRoutingModule,
    ReactiveFormsModule,
    PipeModule,
    NgxMaskModule.forRoot(),
    DirectivesModule
  ]
})
export class AddressModule { }
