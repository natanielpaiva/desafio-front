import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpHandler,
    HttpRequest
} from '@angular/common/http';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

    constructor() { }
    /*
        Interceptador simples das requisições http que serão feitas para a API
    */
    intercept(req: HttpRequest<any>, next: HttpHandler) {
        let authReq = req.clone({ setHeaders: { "Authorization": "Basic " + btoa("client" + ':' + "123") } });
        if (!req.url.includes("oauth/token?grant_type=password")) {

            const authToken = "Bearer " + sessionStorage.getItem("access_token");
            authReq = req.clone({ setHeaders: { Authorization: authToken } });
            if (req.url.includes("viacep.com.br/ws/") || req.url.includes("servicodados.ibge.gov.br")) {
                authReq = req.clone({})
            }
        }


        return next.handle(authReq);
    }
}
