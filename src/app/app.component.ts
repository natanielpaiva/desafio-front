import { Component, OnInit } from '@angular/core';
import { LoginService } from './login/shared/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public isLogged = false

  constructor(private loginService:LoginService){
  }

  public ngOnInit(){
    if(sessionStorage.getItem("access_token"))
      this.isLogged = true

    this.loginService.isLogged.subscribe(
      is => this.isLogged = is
    );

  }
}
