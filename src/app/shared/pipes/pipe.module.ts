import { NgModule } from '@angular/core';
import { CepPipe } from './cep.pipe';

@NgModule({
    imports: [],
    declarations: [
        CepPipe
    ],
    exports: [
        CepPipe
    ],
})
export class PipeModule {
    static forRoot() {
        return {
            ngModule: PipeModule,
            providers: [],
        };
    }
}
