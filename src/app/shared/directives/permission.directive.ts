import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { LoginService } from 'src/app/login/shared/login.service';
@Directive({
    selector: '[appPermissionRoles]'
})
export class PermissionDirective {
    constructor(
        private loginService: LoginService,
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef
    ) { }

    @Input() set appPermissionRoles(roles: string[]) {
        if (this.loginService.verifyRoles(roles)) {
            this.viewContainer.createEmbeddedView(this.templateRef);
        } else {
            this.viewContainer.clear();
        }
    }
}
