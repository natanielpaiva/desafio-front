import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PermissionDirective } from './permission.directive';

const DIRECTIVES_COMPONENTS = [
	PermissionDirective
];

@NgModule({
	imports: [
		CommonModule,
	],
	declarations: DIRECTIVES_COMPONENTS,
	exports: DIRECTIVES_COMPONENTS
})
export class DirectivesModule {
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: DirectivesModule
		};
	}
}
