import { Component, OnInit } from '@angular/core';
import { Role } from '../shared/roles/role';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  public roles = [Role.ADMIN]
  constructor() { }

  ngOnInit() {
  }

}
