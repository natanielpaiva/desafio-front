import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login/shared/login.service';
import { Role } from '../shared/roles/role';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public isLogged = false
  public roles = [Role.ADMIN]
  /*
    Todo Component precisa configurar os seus roles de acesso, desde que herde da BaseComponent
  */
  constructor(private loginService: LoginService) {
  }

  logout() {
    this.loginService.logout()
    this.loginService.setAuthenticated(false);
  }

  ngOnInit() {

    if (sessionStorage.getItem("access_token"))
      this.isLogged = true

    this.loginService.isLogged.subscribe(
      is => {
        this.isLogged = is
      }
    );

  }

}
