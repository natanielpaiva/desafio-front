import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UfService {
  private url = environment.ibge

  constructor(private http: HttpClient) { }

  public findAll(): Observable<any> {
    return this.http.get<any>(this.url);
  }
}
