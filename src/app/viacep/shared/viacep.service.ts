import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Address } from '../../address/shared/address';

@Injectable({
  providedIn: 'root'
})
export class ViacepService {

  private url = environment.viacepUrl

  constructor(private http: HttpClient) { }

  public getCep(cep: string): Observable<any> {
    return this.http.get<Address>(this.url + cep  + "/json");
  }
}
