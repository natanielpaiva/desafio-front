import { Injectable, EventEmitter } from '@angular/core'
import { environment } from 'src/environments/environment'
import { HttpClient } from '@angular/common/http'
import { User } from 'src/app/user/shared/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public isLogged = new EventEmitter<boolean>();
  private url = environment.backendUrl + "oauth/token?grant_type=password&username="

  constructor(private http: HttpClient) { }

  public login(user: User): Observable<any> {
    return this.http.post<any>(this.url + user.email + "&password=" +
      encodeURIComponent(user.password), {});
  }
 
  public getUserAuth(): Observable<any> {
    return this.http.get<any>(environment.backendUrl + "user-auth");
  }

  public verifyRoles(roles:string[]){
      let response = false
      for (const role of roles) {
        if (this.isRole(role)) {
          response = true
        }
      }
      return response
  }

  private isRole(roleVerify:string){
    let response = false
    let user = JSON.parse(sessionStorage.getItem("login"))
    user.roles.forEach(role => {
       response = role.name === roleVerify
    });
    return response
  }

  public logout(){
    sessionStorage.clear()
  }

  public setAuthenticated(isAuthenticated: boolean) {
    this.isLogged.emit(isAuthenticated);
  }

}
