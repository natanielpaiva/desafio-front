import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from './shared/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public form: FormGroup
  public invalid = false

  constructor(
    private loginService: LoginService,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  public autenticar() {
    if (!this.form.invalid) {
      this.loginService.login(this.form.value).subscribe(
        response => {
          sessionStorage.setItem("access_token", response.access_token)
          this.getUserAndRedirect()
        }
        , error => {
          this.invalid = true
        }
      );
    }
  }

  private getUserAndRedirect() {
    this.loginService.getUserAuth().subscribe(
      response => {
        this.loginService.setAuthenticated(true)
        sessionStorage.setItem("login", JSON.stringify(response))
        this.router.navigate(['/home']);
      }
    )
  }

  close() {
    this.invalid = false
  }

  get f() { return this.form.controls; }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

}
