/*
  Coloquei a mesma URL, mas pode ser mudada de acordo com cada ambiente
*/
export const environment = {
  production: true,
  backendUrl:"http://bystaffingapi.us-east-2.elasticbeanstalk.com/",
  viacepUrl: "https://viacep.com.br/ws/",
  ibge:"https://servicodados.ibge.gov.br/api/v1/localidades/estados"
};
